#!/bin/sh


#instructions to start the Flask application

export FLASK_APP=app.py
flask db init 
flask db migrate 
flask db upgrade 

exec gunicorn --config /app/gunicorn_config.py wsgi:app
