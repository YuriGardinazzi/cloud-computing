# Strange Blog

## Project description

This project is a strange blog that could be used also as a sort of online Curriculum Vitae.

The application need a database to store information about views of every post but the post itself consists of markdown 
files matching the structure in `services/app/posts/template.md.tmpl`

Blog maintainer will publish new article by simply pushing new Markdown files into `post` folder.

### Project workflow
The blog is really simple and probably bad designed in many ways (but carefully designed for DevOps course purpose).
It consists of 2 main part:
- blog posts
- Post views

#### Blog post
Blog post are published by simply adding a new Markdown file to the folder `services/app/posts/en`.
All the markdown file must follow the default template provided in `services/app/posts/template.md.tmpl`. Specifically, each post 
must have:
- title: blog post title
- subtitle: blog post subtitle
- author: Author Name
- author_image: file_name in static/assets/blog-images for the author profile picture
- date: Date in the format %B %d, %Y
- image: file_name in static/assets/blog-images for the blog picture
- permalink: must be unique
- tags: list,of,comma,separated,tags
- shortcontent: Short abstract of the blog post

finally **three dash** to divide attribute from markdown content, the markdown content below dashes is 
the article real content.

Images are optional and optional parameter must not be inserted in the file.

Every image eventually specified must be placed in the folder `services/app/static/assets/blog-imaes` and commit altogether.

#### Post views
To keep track of each post visualisations, a simple table is created in a postgres database using `Flask-SQLAlchemy`.
The table contain simply the post name, that correspond to the permalink, and an integer for post views.

## Run project

In order to make it work the requisite are
```
./deploy_update.sh
```

