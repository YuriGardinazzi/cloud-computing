


# Idee
# Dizionario in cui ogni elemento  è una coppia [funziona che punta alla regola, stringa

from datetime import datetime
import os
import sys
def check_date(input_text):
    try:
        if(datetime.strptime(input_text.strip(),"%B %d, %Y")):
                return True
    except:
        return False
    return False


def check_if_dateformat(input_text):
    #TODO: improve the control over dateformat
    if(input_text.strip() in "%B %d, %Y"):
        return True
    return False

def check_if_file_extension_format(input_text):
    ris = input_text.strip().split(".")

    if(len(ris) == 2 and ris[1] in "extension"):
        #if(len(input_text.split(".")[1]) in "extension"):
        return True
    return False

def check_file_with_extension(input_text):
    ris = input_text.strip().split(".")
    if(len(ris) == 2 and len(ris[1].split()) == 1):
        #if(len(input_text.split(".")[1]) in "extension"):
        return True

    return False

def check_if_separated_with_comma(input_text):
    ris = input_text.strip().split(',')
    if(len(ris)==1):
        return False
    for word in ris:
        if(len(word.split(" "))>1):
            return False
    return True

def check_string_separated_with_comma(input_text):
    #list of words separated with comas
    
    ris = input_text.strip().split(',')
    for word in ris:
        if(len(word.split(" "))>1):
            return False
    return True

def check_text(input_text):
    if(check_if_separated_with_comma(input_text)):
        return False


    
    if(check_date(input_text)):
        print("there's a date")
        return False    

    
    #checks if the string is an input file
    if(check_file_with_extension(input_text)):
        print("ther's an extension")
        return False
    
    #if(check_string_separated_with_comma(input_text)):
    #    return False
    
    return True


def create_rules_from_template(path):
    template = {}
    with open(path,"r") as f:
        for line in f:
            #line = f.readline()
            if(line.strip() in "---"):
                #f.close()
                break
            
            attribute_name = line[0:line.index(":")].strip()
            attribute_value = line[line.index(":")+1::]

            if(check_if_separated_with_comma(attribute_value)):
                template[attribute_name] = check_string_separated_with_comma
            elif(check_if_file_extension_format(attribute_value)):
                template[attribute_name] = check_file_with_extension
            elif(check_if_dateformat(attribute_value)):
                template[attribute_name] = check_date
            elif(check_text(attribute_value)):
                template[attribute_name] = check_text    

            #print(attribute_name," ", template[attribute_name].__name__)    
    return template

def apply_template(template,path):
    with open(path,"r") as f:
            for key in template:
                line = f.readline()
                if(line.strip() in "---"):
                    f.close()
                    break
                to_evaluate =line[line.index(":")+1::]
                if(not template[key](to_evaluate)):
                    raise ValueError(f"File {path} does not respect the template tag {key}")
                    
                    

    return 
def check_folder_with_template(template,dir_path):
    
    files_to_check = []
    
    with open("/var/tmp/list","r") as f:
        for line in f:
            files_to_check.append(line.strip())
        #print("file found?")
    """
    except:
        print("file not found")        
    if "MD_NEW_FILES" in os.environ:
        print("MD_NEW_FILES "+os.getenv("MD_NEW_FILES"))
    else:
        print("MD_NEW_FILES not found :( env: ", len(os.environ))
        #for a in os.environ:
        #    print('Var: ', a, 'Value: ', os.getenv(a))
    """
    print("File to check: ",len(files_to_check))
    print(files_to_check)
    for file_path in files_to_check:
        if os.path.isfile(os.path.join(file_path)):
            try:
                print(f"trying {file_path}")
                ris = apply_template(template,file_path)
            except ValueError as err:
                raise ValueError(f"{err}")
    """
    for file_path in os.listdir(dir_path):
        # check if current path is a file
        if os.path.isfile(os.path.join(dir_path, file_path)):
            try:
                print(f"trying {file_path}")
                ris = apply_template(template,dir_path+os.sep+file_path)
            except ValueError as err:
                raise ValueError(f"{err}")
    """            
    return

if __name__ == "__main__":
    template = create_rules_from_template("services/app/posts/template.md.tmpl")
    try:
        check_folder_with_template(template,"services/app/posts/en")
        #ris = apply_template(template,"services/app/posts/en/my-first-freelance-exp.md")
    except Exception as err:
        print(f"Errore nella correzione del template: {err}")
        sys.exit(1)
